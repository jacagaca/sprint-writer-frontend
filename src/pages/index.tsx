import {
  Link as ChakraLink,
  Text,
  Code,
  Icon,
  List,
  ListIcon,
  ListItem,
} from "@chakra-ui/core";

import { Footer } from "../components/Footer";
import NavBar from "../components/NavBar";
import { withUrqlClient } from "next-urql";
import { createUrqlClient } from "../utils/createUrqlClient";
import { usePostsQuery } from "../generated/graphql";

const Index = () => {
  const [{ data }] = usePostsQuery();
  console.log(data?.posts);
  return (
    <div>
      <NavBar></NavBar>
      {!data?.posts ? (
        <div> loading... </div>
      ) : (
        data?.posts.map((post) => <div key={post.id}>{post.text}</div>)
      )}
      <Footer>
        <Text>Next ❤️ Chakra</Text>
      </Footer>
    </div>
  );
};

export default withUrqlClient(createUrqlClient, { ssr: true })(Index);
