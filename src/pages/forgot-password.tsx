import React, { useState } from "react";
import Wrapper from "../components/Wrapper";
import { Formik, Form } from "formik";
import { Box, Link, Button } from "@chakra-ui/core";
import { InputField } from "../components/InputField";
import { withUrqlClient } from "next-urql";
import { createUrqlClient } from "../utils/createUrqlClient";
import { useForgotPasswordMutation } from "../generated/graphql";
import { useRouter } from "next/router";
import NextLink from "next/link";
const ForgotPassword: React.FC<{}> = ({}) => {
  const router = useRouter();
  const [_, forgotPassword] = useForgotPasswordMutation();
  const [goodMessage, setGoodMessage] = useState("");
  const [badMessage, setBadMessage] = useState("");
  return (
    <Wrapper variant="small">
      <Formik
        initialValues={{ email: "" }}
        onSubmit={async (values) => {
          const response = await forgotPassword({
            email: values.email,
          });
          console.log(response);
          if (response.data?.forgotPassword) {
            setGoodMessage(
              "New link to change your password has been send to your e-mail address. Redirect to main page in 3 seconds."
            );
            setBadMessage("");
            setTimeout(() => {
              router.push("/");
            }, 3000);
          } else {
            setBadMessage("Incorrect e-mail address.");
            setGoodMessage("");
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Box mt={4}>
              <InputField
                name="email"
                placeholder="E-mail address"
                label="E-mail address"
                type="email"
              />
            </Box>
            <Box>
              <Box style={{ color: "red" }}>{badMessage ? badMessage : ""}</Box>
              <Box style={{ color: "green" }}>
                {goodMessage ? goodMessage : ""}
              </Box>
              <NextLink href="/">
                <Link>Go back to main page</Link>
              </NextLink>
            </Box>

            <Button
              mt={4}
              type="submit"
              variantColor="green"
              isLoading={isSubmitting}
            >
              SEND CHANGE PASSWORD LINK
            </Button>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
};

export default withUrqlClient(createUrqlClient)(ForgotPassword);
