import React from "react";
import { Button, Box, Link, Flex } from "@chakra-ui/core";
import { Formik, Form } from "formik";
import Wrapper from "../components/Wrapper";
import { InputField } from "../components/InputField";

import { useLoginUserMutation } from "../generated/graphql";
import { useRouter } from "next/router";
import { withUrqlClient } from "next-urql";
import { createUrqlClient } from "../utils/createUrqlClient";
import NextLink from "next/link";
interface loginProps {}

const Login: React.FC<loginProps> = ({}) => {
  const [_, loginUser] = useLoginUserMutation();
  const router = useRouter();
  return (
    <Wrapper variant="small">
      <Formik
        initialValues={{ email: "", password: "" }}
        onSubmit={async (values, { setErrors }) => {
          const response = await loginUser({
            email: values.email,
            password: values.password,
          });

          if (response.data?.loginUser.errors) {
            const name = response.data.loginUser.errors[0].field;
            const message = response.data.loginUser.errors[0].message;

            var error = {
              [name]: message,
            };

            setErrors(error);
          } else if (response.data?.loginUser.user) {
            router.push("/");
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Box mt={4}>
              <InputField
                name="email"
                placeholder="Email"
                label="Email"
                type="text"
              />
            </Box>

            <Box mt={4}>
              <InputField
                name="password"
                placeholder="Password"
                label="Password"
                type="password"
              />
            </Box>

            <Button
              mt={4}
              type="submit"
              variantColor="green"
              isLoading={isSubmitting}
            >
              LOGIN
            </Button>
            <Flex>
              <NextLink href="/forgot-password">
                <Link ml="auto">Forgot password?</Link>
              </NextLink>
            </Flex>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
};

export default withUrqlClient(createUrqlClient)(Login);
