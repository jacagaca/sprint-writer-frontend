import React from "react";

import { NextPage } from "next";
import Wrapper from "../../components/Wrapper";
import { Formik, Form } from "formik";
import { Box, Button, Link } from "@chakra-ui/core";
import { InputField } from "../../components/InputField";
import { useRouter } from "next/router";
import { useChangePasswordMutation } from "../../generated/graphql";
import { withUrqlClient } from "next-urql";
import { createUrqlClient } from "../../utils/createUrqlClient";
import NextLink from "next/link";

const ChangePassword: NextPage<{ token: string }> = ({ token }) => {
  const router = useRouter();

  const [_, changePassword] = useChangePasswordMutation();
  return (
    <Wrapper variant="small">
      <Formik
        initialValues={{ newPassword: "" }}
        onSubmit={async (values, { setErrors }) => {
          const response = await changePassword({
            token: token,
            newPassword: values.newPassword,
          });
          if (response.data?.changePassword.errors) {
            const name = response.data.changePassword.errors[0].field;
            const message = response.data.changePassword.errors[0].message;
            let error;
            if (!message.includes("token")) {
              error = {
                [name]: message,
              };
            } else {
              error = {
                [name]: message,
              };
            }

            setErrors(error);
          } else if (response.data?.changePassword.user) {
            router.push("/");
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Box mt={4}>
              <InputField
                name="newPassword"
                placeholder="New password"
                label="New password"
                type="password"
              />
            </Box>
            <Box>
              <NextLink href="/forgot-password">
                <Link>Click here to get new one.</Link>
              </NextLink>
            </Box>
            <Button
              mt={4}
              type="submit"
              variantColor="green"
              isLoading={isSubmitting}
            >
              CHANGE PASSWORD
            </Button>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
};

ChangePassword.getInitialProps = ({ query }) => {
  return {
    token: query.token as string,
  };
};

export default withUrqlClient(createUrqlClient)(ChangePassword);
