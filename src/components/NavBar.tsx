import React from "react";
import { Box, Link, Flex, Button } from "@chakra-ui/core";
import { DarkModeSwitch } from "../components/DarkModeSwitch";
import NextLink from "next/link";
import { useMeQuery } from "../generated/graphql";
import { useLogoutUserMutation } from "../generated/graphql";
import { isServer } from "../utils/isServer";

interface navBarProps {}

const NavBar: React.FC<navBarProps> = ({}) => {
  const [{ data, fetching }] = useMeQuery({
    pause: isServer(),
  });
  let body = null;
  const [logoutUserData, logoutUser] = useLogoutUserMutation();

  if (fetching) {
    body = null;
  } else if (!data?.me) {
    body = (
      <>
        <NextLink href="/register">
          <Link color="white" mr={12}>
            Register
          </Link>
        </NextLink>
        <NextLink href="/login">
          <Link color="white" mr={12}>
            Login
          </Link>
        </NextLink>
      </>
    );
  } else {
    body = (
      <Flex>
        <Box mr={4}>{data?.me?.username}</Box>
        <Button
          isLoading={logoutUserData.fetching}
          onClick={() => logoutUser()}
          mr={4}
          variant="link"
        >
          Logout
        </Button>
      </Flex>
    );
  }

  return (
    <Flex bg="tomato" p={4}>
      <Box marginLeft={"auto"} mr={10}>
        {body}
        <DarkModeSwitch />
      </Box>
    </Flex>
  );
};

export default NavBar;
