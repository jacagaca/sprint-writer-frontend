import { MeDocument } from "../generated/graphql";

import { Provider, createClient, dedupExchange, fetchExchange } from "urql";
import { cacheExchange, Data } from "@urql/exchange-graphcache";

export const createUrqlClient = (ssrExchange: any) => ({
  url: "http://localhost:4000/graphql",
  fetchOptions: { credentials: "include" as const },
  exchanges: [
    dedupExchange,
    cacheExchange({
      updates: {
        Mutation: {
          loginUser: (result: Data | any, args, cache, info) => {
            cache.updateQuery({ query: MeDocument }, (data: Data | null) => {
              if (result?.loginUser?.errors) {
                return null;
              } else if (data) {
                data.me = result?.loginUser?.user;
                return data;
              } else {
                return data;
              }
            });
          },
          registerUser: (result: Data | any, args, cache, info) => {
            cache.updateQuery({ query: MeDocument }, (data: Data | null) => {
              if (result?.registerUser?.errors) {
                return null;
              } else if (data) {
                data.me = result?.registerUser?.user;
                return data;
              } else {
                return data;
              }
            });
          },
          logoutUser: (result: Data | any, args, cache, info) => {
            cache.updateQuery({ query: MeDocument }, (data: Data | null) => {
              if (data) {
                data.me = null;

                return data;
              } else {
                console.log(data);
                return data;
              }
            });
          },
        },
      },
    }),
    ssrExchange,
    fetchExchange,
  ],
});
